<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />


    <title>Negocios Verdes</title>
  </head>
  <body>

       <div class="contenedor">

            <nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #0093D8;">
                <a class="navbar-brand" href="index.php"><img src="img/logo.png" class="img-fluid" alt="" style="margin-left: 10px;"></a>
                <button class="navbar-toggler" style="background-color:#84C722;" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.php" style="color: white;"><i class="fas fa-home" style="margin-right: 5px;"></i><strong>Inicio</strong></a>
                        </li>

                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                        <i class="fas fa-info" style="margin-right: 5px;"></i><strong>Quienes somos</strong>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="about.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Misión y Visión</a>
                            <a class="dropdown-item" href="normatividad.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Normatividad</a>
                            <a class="dropdown-item" href="mecanismos.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Mecanismo de articulación</a>
                            <a class="dropdown-item" href="antecedentes.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Antecedentes</a>
                            <a class="dropdown-item" href="galeria.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Galería</a>
                            
                        </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                            <i class="far fa-newspaper" style="margin-right: 5px;"></i><strong>Negocios Verdes</strong>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="negocios.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Qué son</a>
                                <a class="dropdown-item" href="categorias.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Categorias y Sectores</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
                            <i class="fas fa-check" style="margin-right: 5px;"></i><strong>Criterios</strong>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="criterios.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Criterios de negocios verdes</a>
                                <a class="dropdown-item" href="que_tan_verde.php"><i class="fas fa-plus-circle" style="color:#84C722; margin-right: 15px;"></i>Que tan verde es mi negocio</a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="registro.php" style="color: white;"><i class="fas fa-user-plus" style="margin-right: 5px;"></i><strong>Registrate</strong></a>
                        </li>
            
                        <li class="nav-item">
                            <a class="nav-link" href="noticias.php" style="color: white;"><i class="fas fa-newspaper" style="margin-right: 5px;"></i><strong>Noticias</strong></a>
                        </li>
            
                        <li class="nav-item">
                            <a class="nav-link" href="contacto.php" style="color: white;"><i class="fas fa-id-card-alt" style="margin-right: 5px;"></i><strong>Contacto</strong></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="https://kadri.dongee.com:2083/" style="color: white;"><i class="fas fa-envelope-open-text" style="margin-right: 5px;"></i><strong>Correo</strong></a>
                        </li>

                    </ul>

                </div>
                
            </nav>

            <hr style="margin-top: 200px;">

            <hr>