<?php
include('header.php');
?>


           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Contáctanos</strong></h2>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                    Estamos para ayudarte en lo que necesites, Contáctanos para obtener mas información acerca de nuestra ventanilla de negocios verdes.
                    </p>
                    
                <!--registro-->

                <div class="negocios">

                    <div class="container-description">
                        

                        <form action="contactar.php" method="POST">
                            
                            <div class="form-row">

                                <div class="form-group col-sm-6">
                                    <label for="">Nombre</label>
                                    <input type="text" class="form-control" name="nombre" placeholder="Ingresa Nombre" required>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="apellidos" placeholder="Ingresa Apellido" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-sm-8">
                                    <label for="">Correo</label>
                                    <input type="email" class="form-control" name="correo" placeholder="Ingresa Correo" required>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="">N° de Celular</label>
                                    <input type="number" class="form-control" name="celular" placeholder="Ingresa su N° de celular" required>
                                </div>

                            </div>


                            <div class="form-row">

                                <div class="form-group col-sm-9">
                                    <label form="">Direccion</label>
                                    <input type="text" class="form-control" name="direccion" placeholder="Ingrese du Dirección" required>
                                </div>

                                <div class="form-group col-sm-3">
                                    <label form="ciudad">Ciudad</label>
                                    <input type="text" class="form-control" name="ciudad" placeholder="Ingrese su ciudad" required>
                                </div>

                            </div>

                            <div class="form-group">
                                <label for="">Comentario</label>
                                <textarea id="comentario" name="comentario" class="form-control" rows="5" required></textarea>
                            </div>

                            <div class="form-group form-check">

                            <p>
                                    Autoriza a el tratamiento de acuerdo con los fines establecidos y la seguridad y privacidad de la información que recolecte, almacene, 
                                    use, circule o suprima, que contenga datos personales y en cumplimiento del mandato legal, establecido en la Constitución Política de 
                                    Colombia(arts. 15 y 20), la Ley 1581 de 2012 "por la cual se dictan disposiciones generales para la protección de datos personales" y 
                                    el Decreto 1377 de 2013 "por el cual se reglamenta parcialmente la Ley 1581 de 2012" y al compromiso institucional en cuanto al tratamiento de la información. 
                                    </p>

                                <input type="checkbox" class="form-check-input" id="contactame" required style="margin-left: 5px;">
                                <label for="contactame" class="form-check-label" style="margin-left: 20px;">Acepto</label>
                            </div>

                            <button type="submit" class="btn btn-primary" style="display: block; margin:auto;" id="enviar2">Enviar</button>

                        </form>

                    </div>

                </div>

                <!--fin registro-->


                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">


                </div>


            </div>


           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="js/sweetAlert.js"></script>



  </body>
</html>