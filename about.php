<?php
include('header.php');
?>


           <!-- QUIENES SOMOS-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Quienes somos</strong></h2>
                    </div>

                    <div class="container-content">

                        <div class="content">
                             <p>
                                La Corporación Autónoma Regional del Cesar, Corpocesar, 
                                es un ente corporativo de carácter público encargado de 
                                administrar el medio ambiente y propender por el 
                                desarrollo sostenible del departamento del Cesar.
                            </p>

                            <p>
                                Es un ente del orden nacional, que como su nombre lo 
                                indica, cuenta con una autonomía administrativa y 
                                financiera, así como patrimonio propio y persona jurídica. 
                                Fue creada por la Ley 28 28 de 1988 y modificada en 
                                jurisdicción y denominada Ley 99 de 1993.

                            </p>

                            <p>
                                Corpocesar, abarca todo el territorio del departamento del Cesar conformado por 25 municipios, por lo cual viene implementando el Programa Regional de Negocios Verdes, como hoja de ruta para   Sostenible.
                            </p>

                       </div>

                            <div class="container-img">
                              <img src="img/l2.PNG" alt="" class="img-fluid l2">
                            </div>

                    </div>

                </div>

            </div>

           <!--fin QUIENES SOMOS-->

           <hr>

            <!--cards-->

                <div class="container-cards" style="display: flex;">

                    <!-- Columns are always 50% wide, on mobile and desktop -->
                    <div class="row">

                      <div class="col-md-6">

                            <div class="card card1" style="width: 25rem;">

                                <div class="card-body">
                                    
                                    <div class="title5">
                                        <h1 class="t5"><strong>Misión </strong></h1>
                                    </div>

                                </div>

                            
                                <div class="card-body">

                                    <p class="card-text" style="text-align: center; margin-top: 50px">"Liderar dentro del marco del desarrollo sostenible la gestión ambiental en su jurisdicción". </p>

                                </div>

                            </div>

                      </div>

                      <div class="col-md-6">

                                <div class="card card1" style="width: 25rem;">

                                    <div class="card-body">
                                        
                                        <div class="title5">
                                            <h1 class="t5"><strong>Visión </strong></h1>
                                        </div>

                                    </div>

                                
                                    <div class="card-body">

                                        <p class="card-text" style="text-align: center;">"Lograr que en el 2020 el desarrollo integral de la comunidad se dé en armonía con la naturaleza, reconociendo y fortaleciendo la identidad cultural y la vocación productiva del territorio".</p>

                                    </div>
                                    
                                   

                                </div>

                        </div>

                    </div>

                  </div>

            <!--fin cards-->

            <hr>

            <!-- principios-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Principios y valores corporativos</strong></h2>
                    </div>

                    <div class="container-content">

                        <div class="content">

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Calidad en el servicio: Nos esforzamos por el permanente mejoramiento de nuestros servicios formando un equipo humano de alto nivel técnico y profesional, que brinde seguridad y confianza a nuestros usuarios.                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Respeto al ambiente: Creamos en nuestros servidores públicos un alto sentido de responsabilidad frente a la misión de la Corporación para promover el respeto y el compromiso con el medio ambiente y nuestra comunidad.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Contribución participativa de la entidad: Desarrollamos canales apropiados de comunicación y enlace para garantizar la efectiva participación de nuestros servidores públicos y usuarios en el seguimiento y mejoramiento de la gestión de la Corporación.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Compromiso con el quehacer institucional: Nos sentimos plenamente identificados con Corpocesar, es decir con su misión, sus valores, programas y proyectos, como fundamentos legítimos para responder a nuestro compromiso como sociedad.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Gestión ambiental autónoma: Adaptamos nuestra gestión a las diversidades socioculturales y biofísicas que caracterizan nuestra sociedad y su territorio, a través de un trabajo institucional coordinado, que apoye el fortalecimiento de la gestión ambiental responsable y autónoma de las entidades territoriales.
                            </p>


                       </div>

                       <div class="container-img">
                         <img src="img/l3.PNG" alt="" class="img-fluid l2" style="height: 50%; margin: auto;">
                       </div>

                    </div>

                </div>

            </div>

           <!--principios-->
  
           
         

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>