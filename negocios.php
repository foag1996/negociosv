<?php
include('header.php');
?>
            

           <!-- negocios verdes-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>¿Qué son los negocios verdes?</strong></h2>
                    </div>

                    <div class="container-content">

                        <div class="content">
                             <p style="text-align: justify;">
                             Contempla las actividades económicas en las que se ofertan bienes o servicios, que generan impactos ambientales positivos y además incorporan buenas prácticas ambientales, sociales y económicas con enfoque de ciclo de vida, contribuyendo a la conservación del ambiente como capital natural que soporta el desarrollo del territorio.
                             Identificación de los bienes y servicios de negocios verdes y sostenibles.
                             Es relevante porque:</p>

                             <div class="container">

                                <div class="row">
                                    <div class="col-sm" style="border-radius:19px; border: solid #0093D8; margin-top:10px;">
                                    Promueve patrones de producción y consumo sostenibles de bienes y servicios de los negocios verdes y sostenibles.
                                    </div>
                                    <div class="col-sm" style="border-radius:19px; border: solid #0093D8;margin-left:10px; margin-top:10px;">
                                    Propicia la creación de  una cultura alineada con principios ambientales, sociales y éticos.
                                    </div>
                                    <div class="col-sm" style="border-radius:19px; border: solid #0093D8;margin-left:10px; margin-top:10px;">
                                    Facilita la toma de decisiones a los consumidores (públicos o privados) al momento de elegir un bien y servicio.
                                    </div>
                                    <div class="col-sm" style="border-radius:19px; border: solid #0093D8;margin-left:10px; margin-top:10px;">
                                    Visibiliza una oferta de bienes y servicios de cara al mercado nacional e internacional.
                                    </div>
                                </div>
                            </div>

                       </div>

                            <div class="container-img">
                              <img src="img/l2.PNG" alt="" class="img-fluid l2">
                            </div>

                    </div>

                </div>

            </div>

           <!--fin negocios verdes-->

           <hr>

            
           <div class="negocios">

                <div class="container-description">  

                    <div class="title2">
                            <h2 class="t2"><strong>Algunos de nuestros productos</strong></h2>
                    </div> <br>

                    <p style="text-align: justify;">
                    La creación de una oferta de productos  y servicios sostenibles que generan impactos positivos al medio ambiente y a la salud humana y posicionan al departamento del Cesar como un  proveedor de negocios verdes a nivel nacional.
                    </p>

                    <div class="container">

                        <div class="row">

                            <div class="col-sm">
                            <img src="img/t1.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                            <div class="col-sm">
                            <img src="img/t2.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                            <div class="col-sm">
                            <img src="img/t3.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm">
                            <img src="img/t4.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                            <div class="col-sm">
                            <img src="img/t5.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                            <div class="col-sm">
                            <img src="img/t6.png" alt="" class="img-fluid" 
                            style="border-radius:19px; border: solid #0093D8; color:white; margin-left:10px; margin-top:20px;">
                            </div>

                        </div>

                    </div>

                </div>

           </div>

         

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>