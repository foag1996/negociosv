<?php
include('header.php');
?>
            
           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Criterios de Negocios Verdes</strong></h2>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                             Se entiende como “criterio” una categoría que agrupa atributos o características relacionadas o similares. 
                             En este caso, entonces, los criterios señalarán los aspectos del bien o servicio que son fundamentales en 
                             el contexto de la sostenibilidad y que permitirán identificar una oferta de bienes y servicios de Negocios Verdes que, 
                             sin disminuir sus características de calidad, procurando no causar efectos indeseables en el entorno físico y social y 
                             generar unos impactos ambientales positivos directos. Además de informar al consumidor y brindarle herramientas para que 
                             pueda ejercer su derecho a un consumo responsable, con pleno conocimiento del impacto de sus objetos de compra, los criterios 
                             permiten apreciar las relaciones vitales del bien o servicio con aspectos como biodiversidad, productividad, protección de los 
                             recursos renovables y no renovables, a través de su uso eficiente, y representan un beneficio per se al productor, al consumidor 
                             y a la sociedad misma.
                            </p>

                            <p>Algunas de las características de los criterios de negocios verdes se presentan a continuación:</p>

                            <div class="container">

                                <div class="row">

                                    <div class="col-sm 1"  
                                        style="text-align: justify; border-radius:15px; 
                                        border: solid #84C722; margin-top:20px; margin-left:20px; 
                                        margin-right:20px; background-color:#0093D8; color:white;">

                                        <p>
                                        Es importante aclarar que no se considerarán como criterios aspectos ambientales y sociales que sean requisitos de ley, 
                                        ya que se entiende que el mínimo requerido es cumplir con la legislación nacional e internacional aplicable.</p>

                                    </div>


                                </div>


                            </div>

                            <div class="container">

                            <div class="row">

                                <div class="col-sm 1"  
                                    style="text-align: justify; border-radius:15px; 
                                    border: solid #84C722; margin-top:20px; margin-left:20px; 
                                    margin-right:20px; background-color:#0093D8; color:white;">

                                    <p>
                                    Los criterios que se mencionan a continuación son implementados de manera adicional y voluntaria por las empresas, 
                                    buscando dar un valor agregado a sus bienes y servicios considerados como Negocios Verdes y Sostenibles.</p>

                                </div>

                            
                            </div>

                            
                        </div>


                </div>

            </div>

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>A continuación, se detallan los criterios de negocios verdes:</strong></h2>
                    </div>

                    <br>

                    <p style="text-align: justify;">
                    Los criterios que se mencionan a continuación son implementados de manera adicional y voluntaria por 
                    las empresas, buscando dar un valor agregado a sus bienes y servicios considerados como Negocios Verdes y Sostenibles.
                    </p>
                    
                    <br>

                    <div class="container">

                        <div class="row">

                            <div class="col-sm">

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Viabilidad económica del negocio.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Impacto ambiental positivo del bien o servicio.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Enfoque de ciclo de vida del bien o servicio.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Esquemas, programas o reconocimientos ambientales o sociales implementados o recibidos.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                No uso sustancias o materiales peligrosos.
                                </p>
                                
                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Reciclabilidad de los materiales y/o uso de materiales reciclados.
                                </p>

                            </div>


                            <div class="col-sm">

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Vida útil.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Uso eficiente y sostenible de recursos para la producción del bien o serviciovicio.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Responsabilidad social al interior de la empresa.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Responsabilidad social y ambiental en la cadena de valor de la empresa.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Responsabilidad social y ambiental al exterior de la empresa.
                                </p>

                                <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Comunicación de atributos sociales o ambientales asociados al bien o servicio.
                                </p>

                                
                                        
                            </div>
                            
                        </div>
                        

                    </div>

                    

                    
                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">
                   

                </div>

               

            </div>

            

           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>