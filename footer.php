<footer class="footer">
                    <!-- Grid container -->
                    <div class="container p-4">
                    <!--Grid row-->
                    <div class="row">
                        <!--Grid column-->
                        <div class="col-4" style="text-align: left;">

                
                        <p>
                            Lunes a Viernes de 8:00 A.M. a 5:00 P.M
                            Correo: pmlrespel@corpocesar.gov.co
                            Edificio Bioclimático de Corpocesar, Km 2 vía La Paz. 
                            Lote 1 U.I.C Casa e´ Campo. Frente a la feria ganadera 
                            Valledupar- Cesar.

                        </p>
                        </div>
                        <!--Grid column-->
                
                        <!--Grid column-->
                        <div class="col-4">
                           <img src="img/l3.png" class="img-fluid" alt="">
                        </div>
                        <!--Grid column-->
                
                        <!--Grid column-->
                        <div class="container-footer">
                            <div class="col-4">
                                <!-- <p class="text mb-0"  >Siguenos en nuestras redes sociales</p> -->
                                <div class="social-bar2">
                                    <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                                    <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                                    <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                                    <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
                                </div>
            
                            </div>
                        </div>
                        </div>
                        <!--Grid column-->
                    </div>
                    <!--Grid row-->
                    </div>
                    <!-- Grid container -->
                
                        <!-- Copyright -->
                        <div class="text-center p-3 copy">
                        © 2021 Copyright - negociosverdescesar.com.co
                        <p>ING. Fabian Alvarez.</p> 
                        
                        </div>
                        <!-- Copyright -->
                </footer>