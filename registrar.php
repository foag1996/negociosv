<?php

include('generate_exel.php');
include('mail_functions.php');


include ("conexion.php");


$nombre = $_POST["nombre"];
$apellidos = $_POST["apellidos"];
$cedula = $_POST["cedula"];
$celular = $_POST["celular"];
$correo = $_POST["correo"];
$direccion = $_POST["direccion"];
$razon = $_POST["razon"];
$rut = $_POST["rut"];
$actividad = $_POST["actividad"];
$servicio = $_POST["servicio"];
$facebook = $_POST["facebook"];
$instagram = $_POST['instagram'];
$twitter = $_POST['twitter'];
$pagina_web = $_POST['pagina_web'];
$youtube = $_POST['youtube'];
$ingresos2019 = $_POST["valores2019"];
$ingresos2020 = $_POST['valores2020'];


if ($_FILES["archivo"]) {
    // archivo  -------- camara de comercio
    $nombre_base = basename($_FILES["archivo"]["name"]);
    $nombre_final = date("m-d-y"). "-". date("H-i-s"). "-" .$nombre_base;
    $ruta = "archivos/" . $nombre_final;
    $subirarchivo = move_uploaded_file ($_FILES["archivo"]["tmp_name"], $ruta);
    
    // archivo_rut
    $nombre_base2 = basename($_FILES["archivo_rut"]["name"]);
    $nombre_final2 = date("m-d-y"). "-". date("H-i-s"). "-" .$nombre_base2;
    $ruta2 = "archivos/" . $nombre_final2;
    $subirarchivo2 = move_uploaded_file ($_FILES["archivo_rut"]["tmp_name"], $ruta2);

    if ($subirarchivo){
        $insertarSQL = "INSERT INTO registro(nombre, apellidos, cedula, celular,correo, direccion, razon, rut, actividad, servicio, archivo,
        facebook, instagram, tweeter, pagina_web, youtube, ingresos2019, ingresos2020, archivo_rut) 
         VALUES ('$nombre', '$apellidos', '$cedula', '$celular','$correo', '$direccion', '$razon', '$rut', '$actividad', '$servicio','$ruta',
         '$facebook', '$instagram', '$twitter','$pagina_web', '$youtube', '$ingresos2019', '$ingresos2020', '$ruta2' )";
        $resultado = mysqli_query($conexion, $insertarSQL);
        if($resultado){
            $path = makeExcelRegister($nombre,$apellidos,$cedula,$celular,
                        $correo,$direccion,$razon,$rut,$actividad,$servicio,$ruta,
                        $facebook, $instagram, $twitter,$pagina_web, $youtube, $ingresos2019, $ingresos2020, $ruta2);
            $body = '<html>
            <head>		
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            </head>
            <body>
                <div id="psdg-footer">
                    <h2> Nueva solicitud de negocio verde </h2>
                    <br>
                    ---------------------------------------------------------------------
                    <br>
                    <p>
                    Nombre: 	'	. $nombre 	. 	' ' . $apellidos.'<br><br>
                    Telefono: 	' 	. $celular	. 	'<br><br>
                    Email: 		' 	. $correo	.	'<br><br>
                    Direccion: 		' 	. $direccion	.	'<br><br>
                    </p>
                    <p>
                    Rut: 	    ' 	. $rut	. 	'<br><br>
                    Cedula:   	' 	. $cedula	. 	'<br><br>
                    Razon:      ' 	. $razon	. 	'<br><br>
                    </p>
                    <p>
                    Facebook: 	    ' 	. $facebook	. 	'<br><br>
                    Instagram:   	' 	. $instagram	. 	'<br><br>
                    Twitter:      ' 	. $twitter	. 	'<br><br>
                    Youtube:      ' 	. $youtube	. 	'<br><br>
                    Sitio web:      ' 	. $pagina_web	. 	'<br><br>
                    </p>
                    <p>
                    Valores 2019:   	' 	. $ingresos2019	. 	'<br><br>
                    Valores 2020:      ' 	. $ingresos2020	. 	'<br><br>
                    </p>
                    <p>
                    <strong>Actividad:</strong>   '.$actividad.'
                    </p>
                    <p>
                    <strong>Servicio:</strong>     '.$servicio.'
                    </p>
                    
                    <br/>
                    <br/>
                </div>	
            </body>
            </html>';
//
            
            sendMail(['ventanilla@negociosverdescesar.com.co', 'pmlrespel@corpocesar.gov.co'], 'Nueva solicitud de Negocios Verdes', $body, [$path, $ruta, $ruta2], 'info@negociosverdescesar.com.co');
            echo "<script>alert('Te has registrado correctamente'); window.location='index.php'</script>";

        } else {
            printf("Errormessage: %s\n", mysqli_error($conexion));
        }
    }
} else {
    echo "Error al subir el archivo";
}