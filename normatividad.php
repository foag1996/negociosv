<?php
include('header.php');
?>

           <!-- normatividad-->

            <div class="negocios">

                <div class="container-description">

                    <img src="img/n1.jpg.png" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid"> <br>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                        Política de producción y consumo sostenible “PyCS”, hacia una cultura de consumo
                        sostenible y transformación productiva, emitida por el Ministerio de Ambiente y
                        Desarrollo Sostenible en el año 2010, con el fin de promover y enlazar el mejoramiento
                        ambiental y la transformación productiva a la competitividad empresarial (Ministerio de
                        Ambiente, Vivienda y Desarrollo Territorial, 2010).
                        <br>
                        <p>La Política de Producción y Consumo Sostenible "PyCS", con su implementación busca
                        orientar y cambiar los patrones insostenibles de producción y consumo por parte de los
                        diferentes actores de la sociedad nacional, lo que contribuye a reducir la contaminación,
                        conservar los recursos, favorece la integridad ambiental de los bienes y servicios y
                        estimula el uso sostenible de la biodiversidad, como fuentes de la competitividad
                        empresarial y de la calidad de vida.
                        </p>
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Diagnóstico de PyCS del departamento del Cesar, elaborado en el año 2013 por la
                    Corporación Autónoma Regional del Cesar CORPOCESAR y el Centro Nacional de
                    Producción Más Limpia y Tecnologías Ambientales CNPMLTA, con el fin de
                    incorporación de conceptos y prácticas orientadas al cumplimiento de la política de PML
                    y consumo sostenible (CNPMLTA, 2013).
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Plan nacional de negocios verdes, emitido en el año 2014 por el Ministerio de Ambiente
                    y Desarrollo Sostenible, busca definir los lineamientos y proporcionar herramientas para
                    la planificación y toma de decisiones que permitan el desarrollo, el fomento y la
                    promoción tanto de la oferta como de la demanda de los Negocios Verdes y Sostenibles
                    en el país.
                    <br>
                        <p>El Plan Nacional de Negocios Verdes contempla como visión para el año 2025 que los
                            Negocios Verdes estarán posicionados y consolidados como un nuevo renglón
                            estratégico de impacto en la Economía Nacional. Es importante resaltar que los
                            Negocios Verdes son producidos por la oferta (empresas, cadenas de valor,
                            productores) y la demanda (consumidores) y no por las instituciones públicas. <strong>Es por
                            ello que la función de las instituciones lideradas por el Ministerio de Ambiente y
                            Desarrollo Sostenible, debe ser la de proveer una plataforma adecuada de políticas,
                            instrumentos, incentivos y coordinación para que el sector privado pueda generar
                            negocios.
                            </strong> 
                            <br>
                            <p>El Programa de Biocomercio Sostenible se enmarca en el Plan Nacional de Negocios
                                Verdes (2014), y se convierte en una estrategia que busca aprovechar las ventajas
                                comparativas del país en cuanto a su biodiversidad, para facilitar la construcción
                                colectiva de negocios sostenibles que sean competitivos y que propendan por la
                                equidad y la justicia social.
                            </p>
                        </p>
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Política de crecimiento verde, fue aprobada por el Consejo Nacional de Política
                    Económica y Social, el día 10 del mes de Julio del año 2018, mediante el Documento
                    CONPES 3934, cuyo objetivo, es impulsar a 2030 el aumento de la productividad y la
                    competitividad económica del país, al tiempo que se asegura el uso sostenible del
                    capital natural y la inclusión social, de manera compatible con el clima.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Resolución 1784 del 28 de Diciembre de 2018, Ventanilla de Negocios Verdes del
                    departamento del Cesar, administrada por Corpocesar. 
                    <br>
                            <p>Dichas políticas han permitido desarrollar mecanismos de innovación en la generación de
                                oportunidades para el aprendizaje y desarrollo de acciones encaminadas en la
                                implementación de la Política de Gestión Ambiental Urbano regional en el departamento
                                del Cesar, por lo cual se requiere sean expuestas a nivel <strong>Nacional</strong>  como experiencias
                                exitosas de CORPOCESAR en la jurisdicción del departamento del Cesar.
                            </p>

                            <p>De acuerdo a las <strong>Bases del Plan Nacional de Desarrollo 2018 – 2022</strong> de la Presidencia de la
                                Republica de Colombia, el Gobierno Nacional busca consolidar la evolución de la política
                                pública ambiental de los últimos quince años, donde se incluyen, entre otras, las
                                recomendaciones de la Misión de Crecimiento Verde, la Política de Crecimiento Verde, y las
                                políticas, estrategias y planes en materia de economía circular, aire, recurso hídrico, mares
                                y costas, suelo, biodiversidad, cambio climático, gestión del riesgo de desastres, negocios
                                verdes, educación y participación ambiental. Igualmente, busca implementar los
                                instrumentos vigentes a nivel internacional y nacional para el desarrollo sostenible del país.
                            </p>

                    </p>

                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">
                    

                </div>

            </div>

            <hr>

           <!--fin normatividad-->


           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>