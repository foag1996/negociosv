<?php
include('header.php');
?>
    

           <!-- negocios verdes-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Categorías y Sectores de Negocios Verdes</strong></h2>
                    </div>

                    <br>

                    <div class="container-content">

                             <p style="text-align: justify;">
                             Los Negocios Verdes se clasifican en tres (3) categorías y ocho (8) sectores, que pueden ser dinámicos y cambiantes en el tiempo, su característica fundamental es la sostenibilidad y su reglamentación es dada por cada una de las autoridades competentes, dependiendo del tema.</p>

                    </div>

                    <div class="container">

                        <div class="row">

                            <div class="col-sm"  
                            style="text-align: justify; border-radius:15px; border: 
                            solid #707070; margin-top:20px; margin-left:20px; margin-right:20px;">

                                <div class="title2">
                                    <p class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Categorías y Sectores</strong></p>
                                </div>

                                <p>
                                La primera categoría hace referencia a bienes y servicios sostenibles provenientes de recursos naturales, 
                                son aquellos que, en su proceso de aprovechamiento, producción, manejo, transformación, comercialización y/o disposición, 
                                incorporan mejores prácticas ambientales, garantizando la conservación del medio de donde fueron extraídos y la sostenibilidad del 
                                recurso (Oficina de Negocios Verdes del Ministerio de Ambiente y Desarrollo Sostenible, 2014).</p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Negocios para la restauración.                            </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Biocomercio </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Agrosistemas Sostenibles.                            </p>

                            
                            </div>

                            <div class="col-sm"  
                            style="text-align: justify; border-radius:15px; border: solid #707070; margin-top:20px; margin-left:20px;margin-right:20px;">

                                <div class="title2">
                                    <p class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Mercado de Carbono</strong></p>
                                </div>

                                <p>
                                La tercera categoría hace referencia a Mercado de Carbono (relacionado con cambio climático), 
                                son sistemas de comercio a través de los cuales se pueden vender o 
                                adquirir reducciones de emisiones de gases de efecto invernadero (GEI).</p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Negocios para la restauración.                            </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Biocomercio </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Agrosistemas Sostenibles.                            </p>

                            
                            </div>

                            <div class="col-sm"  
                            style="text-align: justify; border-radius:15px; border: solid #707070; margin-top:20px; margin-left:20px;margin-right:20px;">

                                <div class="title2">
                                    <p class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Ecoproductos Industriales</strong></p>
                                </div>

                                <p>
                                La segunda categoría hace referencia a Ecoproductos Industriales, son todos aquellos bienes que pueden demostrar que, en su proceso productivo, 
                                resultan ser menos contaminantes al medio, respecto a otros productos de su segmento; o que por las características intrínsecas del producto, 
                                de su utilización o de su proceso productivo, generan beneficios al ambiente.</p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Aprovechamiento y valorización de residuos.                            </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Fuentes no convencionales de energía renovable. </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Construcción sostenible.                            </p>

                                <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                                Otros Bienes / servicios verdes sostenibles.                            </p>

                            
                            </div>
                            
                        </div>

                        <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">
                    </div>

                </div>

            </div>

           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>