<?php
include('header.php');
?>
        

           <!--mecanismos-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Mecanismos de articulación </strong></h2>
                    </div>

                    <div class="container" style="border-radius:19px; margin-top:20px;">
                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h2 class="t6"><strong>1.Institucionales</strong></h2>
                            </div>

                            </div>
                            <div class="col-sm" style="border: solid #707070; border-radius:19px;">
                            <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Liderar proyectos de Negocios Verdes.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Liderar, articular, coordinar, impulsar, gestionar y estimular a los
                                diferentes actores regionales en la implementación del PRNV.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Posicionar el PRNV, mediante estrategias planeadas entre los
                                beneficiados, haciendo compromisos, evaluando avances y
                                realizar los ajustes que se consideren necesarios durante el
                                proceso.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Crear y consolidar alianzas estratégicas continuas y duraderas
                                que contribuyan a la promoción y posicionamiento del PRNV.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Medir la contribución de programas al desarrollo de la región en
                                cuanto a la conservación de los recursos naturales.</p>
     
                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Servir de instrumento operativo para la dinamización del
                                observatorio de biocomercio sostenible.</p>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h2 class="t6"><strong>2.Oferta</strong></h2>
                            </div>

                            </div>
                            <div class="col-sm" style="border: solid #707070; border-radius:19px;">
                            <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                 Consolidar la oferta de productos verdes regionales.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                 Incentivar la producción de bienes y servicios verdes regionales,
                                 aumentando la competitividad a escala nacional e internacional.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Apoyar a la formulación de proyectos, planes de negocios
                                verdes y planes exportadores.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Promover convenios con universidades o incubadoras para el
                                apoyo a la creación de empresas verdes.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Capacitar a los empresarios en relación a la definición de bienes
                                y servicios verdes.</p>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h2 class="t6"><strong>3.Demanda</strong></h2>
                            </div>

                            </div>
                            <div class="col-sm" style="border: solid #707070; border-radius:19px;">
                            <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Divulgar el potencial y las tendencias de mercadeo de los
                                Negocios Verdes.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Sensibilizar al consumidor sobre la importancia de los productos
                                verdes.</p>

                            <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                Posicionar, conjuntamente con sus aliados, a los Negocios
                                Verdes como su nuevo sector en la economía regional.</p>

                            </div>
                        </div>

                    </div>

                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">
                    

                </div>

            </div>

            
            <hr>

           <!--fin mecanismos-->


           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>