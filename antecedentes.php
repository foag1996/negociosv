<?php
include('header.php');
?>
    
           <!-- normatividad-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>Antecedentes</strong></h2>
                    </div> <br>

                    <p>
                    La Corporación Autónoma Regional del Cesar, CORPOCESAR, es un ente de carácter público
                    encargado de administrar el medio ambiente y propender el desarrollo sostenible en el
                    departamento del Cesar. Además, viene implementando el Programa Fortalecimiento de
                    Negocios Verde con el acompañamiento del Ministerio de Ambiente y la Embajada de la
                    Unión Europea en Colombia
                        <br>
                        <p>En los años 2016 y 2018, CORPOCESAR participó con Negocios Verdes en la Feria
                        Internacional del Medio Ambiente FIMA, logrando presentar propuestas proyectos
                        ambientales que se realizaron con éxito en la ciudad de Bogotá D.C., referente a Producción
                        y Consumo Sostenible, Educación Ambiental, Servicios de Sostenibilidad Ambiental y
                        Negocios Verdes.
                        </p>
                        <p>
                        En el año 2017, CORPOCESAR participó con Negocios Verdes en Bioexpo Colombia, la cual
                        es la única feria donde se encuentra la oferta y demanda de bienes y servicios verdes de
                        todas las regiones del país. Fue un escenario donde se pudo ofrecer negocios sostenibles a
                        nivel internacional, nacional y regional que contribuyen al mejoramiento de la calidad
                        ambiental de la región y el país.
                        </p>
                        <p>
                        En el año 2018 CORPOCESAR constituyó la Ventanilla de Negocios Verdes, a través de la
                        Resolución 1784 del 28 de Diciembre de 2018, la cual es una herramienta que se encuentra
                        en proceso de implementación, y está permitiendo conocer a CORPOCESAR, la oferta de
                        Negocios Verdes en el departamento del Cesar, además estar al tanto de cuál es su
                        desempeño ambiental, social y económico, asimismo está facilitando la construcción de una
                        ruta de acompañamiento y orientación a los empresarios para mejorar el desempeño y
                        posicionamiento de sus iniciativas como Negocios Verdes.
                        </p>
                        <p>
                        CORPOCESAR desde el año 2016 hasta el 2019 ha logrado estimular acciones de mejoras
                        ambientales y verificar un total de sesenta y un (61) unidades productivas de Negocios
                        Verdes en 16 de los 25 municipios departamento del Cesar, de los cuales solo un total de
                        57 cincuenta y siete (57) negocios lograron cumplir con los requisitos y certificarse
                        oficialmente como negocios verde, permitiendo a emprendedores y/o empresarios
                        optimizar el ofrecimiento de bienes y servicios verdes sostenibles en sectores económicos
                        de agrosistemas sostenibles, aprovechamiento y valorización de residuos, Biocomercio,
                        fuentes no convencionales de energía renovable y construcción sostenible, entre otros.
                        </p>
                        <p>
                        Entre los bienes y servicios verdes sostenibles con mayor demanda y oferta identificados
                        por CORPOCESAR en el departamento del Cesar, se encuentran:
                        </p>
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Extracción de miel de abejas para comercialización y servicios naturales de
                    conservación de la especie Apis mellisfera.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Producción de cafés orgánicos especiales, panela orgánica pulverizada de la Sierra
                    Nevada de Santa Marta.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Ecoturismo y aventura, recorridos guiados pedagógicos (cultura, naturaleza e
                    historia) y hospedajes con criterios de sostenibilidad.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Recuperación, transformación y/o valorización de residuos sólidos aprovechables.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Producción de elementos de aseo biodegradables.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Alimentos orgánicos y alternativos (cervezas, conservas, harinas, aceites). 
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Restauración de ecosistemas naturales degradados, a partir de la producción y
                    siembra de materiales vegetales endémicos y silvestres. 
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Fabricación de manillas, llaveros y mochilas arhuacas en lana de ovejos, tejida
                    artesanalmente.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Recuperación de acero, nylon y caucho granulados de neumáticos, para la
                    fabricación de pisos, parques, sillas, entre otros productos.
                    </p>

                    <p><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                    Tiendas verdes de comercialización de productos orgánicos.
                    </p>

                    <p>
                    A nivel nacional a fecha de 2018, se encuentran verificados por el MADS un total de 429
                    Negocios Verdes, y la meta del Gobierno Nacional para el cuatrienio 2018 – 2022, es de
                    <strong>1.865</strong>, con el fin de cumplir pactos transversales por la sostenibilidad: “Producir
                    conservando y conservar produciendo” con sectores comprometidos con la sostenibilidad y
                    la mitigación del cambio climático, dichas actividades están asociadas con los Objetivos de
                    Desarrollo Sostenible “ODS”, como se presenta en la Tabla siguiente:
                    </p>

                    <div class="container-description">
                        

                    <div class="title2">
                        <h2 class="t2"><strong>ODS Asociado</strong></h2>
                    </div>

                    <div class="container" style="border-radius:19px; margin-top:20px;">
                        
                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h2 class="t6"><strong>Primario</strong></h2>
                            </div>

                            </div>

                            <div class="col-sm" style="border: solid #707070; border-radius:19px;">

                                <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                <strong>12.</strong>Garantizar las pautas de consumo y de producción sostenibles.</p>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h2 class="t6"><strong>Secundario</strong></h2>
                            </div>

                            </div>

                            <div class="col-sm" style="border: solid #707070; border-radius:19px;">
                                
                               <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                <strong>8.</strong>Fomentar el crecimiento económico sostenido, inclusivo y
                                sostenible, el empleo pleno y productivo, y el trabajo decente para
                                todos.</p>

                                <p style="margin-top:20px;"><i class="fas fa-plus-circle" style="color: #84C722;"></i>
                                <strong>8.</strong>15. Proteger, restaurar y promover la utilización sostenible de los
                                ecosistemas terrestres, gestionar de manera sostenible los bosques,
                                combatir la desertificación y detener y revertir la degradación de la
                                tierra, y frenar la pérdida de diversidad biológica.</p>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-sm" style="border: solid #707070;  border-radius:19px;" >

                            <div class="title6">
                                <h5 class=""><strong>Fuente:</strong> Bases del Plan Nacional de Desarrollo 2018 – 2022.</h5>
                            </div>

                            </div>
                            
                        </div>

                    </div>
                    <br>

                    <p>
                    Los Negocios Verdes diversifican la economía nacional y generan oportunidades de empleo,
                    potenciando las ventajas comparativas y competitivas de las regiones. El MADS estimó que
                    en el 2017, los 423 negocios verdes verificados por autoridades ambientales generaron
                    alrededor de 118 mil millones de pesos y 6.000 empleos. Por lo cual es necesario consolidar
                    su potencial y requiere instrumentos de financiamiento, formación técnica y tecnológica,
                    apalancamiento comercial e inclusión en cadenas de valor.
                    </p>

                    <p>
                    CORPOCESAR, a través de la Ventanilla Única de Negocios Verdes, realizó Feria de Negocios
                    Verdes, los días 30 y 31 de agosto de 2019, en alianza con el centro comercial Guatapurí
                    Plaza. En la Feria se realizó un Foro de Negocios Verdes, en el cual se transfirieron
                    conocimientos sobre programas de emprendimiento y fomento empresarial de crecimiento
                    verde a nivel local, regional y nacional, con el fin de construir acciones que fortalecieron el
                    desarrollo sostenible en el departamento del Cesar.
                    </p>

                    <p>
                    La Ventanilla de Negocios Verdes de la Corporación realizó el desarrollo de actividad
                    denominada Feria sobre Crecimiento Verde a desarrollarse durante los días 20, 21 y 22 de
                    septiembre de 2019, en Unicentro Valledupar Centro Comercial.
                    </p>

                    <p>
                    CORPOCESAR, participó con seis emprendimientos verdes del departamento del Cesar, en
                    la octava versión de Bioexpo Colombia, la feria más grande de Negocios Verdes y
                    Sostenibles del País que tuvo lugar en la ciudad de Yumbo en el Centro de Eventos Valle del
                    Pacífico, los días 17, 18, 19 y 20 de octubre 2019, fue organizada por el Ministerio de
                    Ambiente y Desarrollo Sostenible y la Corporación Autónoma Regional del Valle del Cauca -
                    CVC, en alianza con la Unión Europea.
                    </p>

                    <p>
                    CORPOCESAR, participó con la presentación de seis negocios verdes debidamente
                    verificados por el Ministerio de Ambiente y Desarrollo Sostenible- MADS- , en este evento
                    que se llevó a cabo del 16 al 18 de Septiembre de 2020 de manera virtual por el canal de
                    YouTube del Ministerio de Ambiente y Desarrollo Sostenible y la página de Facebook de
                    CARDIQUE.
                    </p>

                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">
                    

                </div>

                </div>

            </div>

            <hr>

           <!--fin normatividad-->


           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>