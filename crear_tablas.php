<?php

include ("conexion.php");

$sql1 = "CREATE TABLE IF NOT EXISTS contacto (
    nombre VARCHAR(100), 
    apellidos VARCHAR(100), 
    correo VARCHAR(100), 
    celular VARCHAR(100), 
    direccion VARCHAR(191), 
    ciudad VARCHAR(191),comentario TEXT)ENGINE = InnoDB";


$sql2 = "CREATE TABLE IF NOT EXISTS registro(
    nombre VARCHAR(100), 
    apellidos VARCHAR(100),
    cedula VARCHAR(100),
    celular VARCHAR(100),
    correo VARCHAR(100),
    direccion VARCHAR(100),
    razon VARCHAR(100),
    rut VARCHAR(100),
    actividad VARCHAR(100),
    servicio VARCHAR(100),
    archivo VARCHAR(191)
    )ENGINE=InnoDB"; 


if(!mysqli_query($conexion, $sql1)){
    echo mysqli_error($conexion) . PHP_EOL;
}
else{
    echo "Tabla contacto creada exitosamente" . PHP_EOL;
}

if(!mysqli_query($conexion, $sql2)){
    echo mysqli_error($conexion) . PHP_EOL;
}else{
    echo "Tabla registro creada exitosamente" . PHP_EOL;
}