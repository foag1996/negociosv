<?php
include('header.php');
?>


           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <img src="img/r1.png" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid"> <br>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                    Para Iniciar el proceso de identificación de su iniciativa, por parte de la Ventanilla de Negocios Verdes de Corpocesar, 
                    le pedimos el favor pueda diligenciar el siguiente formulario.
                    </p>
                    
                <!--registro-->

                <div class="negocios">

                    <div class="container-description">
                        

                        <form action="registrar.php" method="POST" enctype="multipart/form-data">

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Nombres</strong></label>
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombres" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Apellidos</strong></label>
                                    <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for=""><strong>N° de Identificacion</strong></label>
                                    <input type="number" class="form-control" name="cedula" placeholder="N° de Identificacion" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for=""><strong>N° de Celular</strong></label>
                                    <input type="number" class="form-control" name="celular" placeholder="N° de Celular" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Correo</strong></label>
                                    <input type="email" class="form-control" name="correo" placeholder="Correo" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Dirección - Municipio</strong></label>
                                    <input type="text" class="form-control" name="direccion" placeholder="Dirección - Municipio" required>
                                </div>


                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Razon Social</strong></label>
                                    <input type="text" class="form-control" name="razon" placeholder="Razon Social" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Número de Identificacion Tributaria (NIT)</strong></label>
                                    <input type="text" class="form-control" name="rut" placeholder="NIT xxx.xxx.xxx-x" required>
                                </div>

                            </div>

                            <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for=""><strong>Redes sociales</strong></label>
                                <input type="text" class="form-control" name="facebook" placeholder="1. Facebook">
                                <input type="text" class="form-control" name="instagram" placeholder="2. Instagram">
                                <input type="text" class="form-control" name="twitter" placeholder="3. Twitter">
                                <input type="text" class="form-control" name="youtube" placeholder="4. Youtube">
                                <input type="text" class="form-control" name="pagina_web" placeholder="5. Pagina Web">
                                
                            </div>

                            <div class="form-group col-md-6">
                                <label for=""><strong>Valores en pesos Colombianos de ingresos por prestación de servicios o ventas</strong></label>
                                <input type="text" class="form-control" name="valores2020" placeholder="1. 2020">
                                <input type="text" class="form-control" name="valores2019" placeholder="2. 2019">
                            </div>


                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Descripción de bienes y servicios ofertados</strong></label>
                                    <textarea name="actividad" class="form-control" rows="5" placeholder="Describa los bienes y servicios ofertados desde su iniciativa económica" required></textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for=""><strong>Impacto ambiental positivo de bienes y servicios ofertados</strong></label>
                                    <textarea name="servicio" class="form-control" rows="5" placeholder="Impacto ambiental positivo de bienes y servicios ofertados" required></textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlFile1">
                                    <p><strong>Adjuntar Archivo Camara de Comercio</strong></p>
                                    <strong>(Documentos Obligatorios)</strong>
                                    </label>
                                    <input type="file" class="form-control-file" name="archivo" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlFile1">
                                    <p><strong>Adjuntar Archivo RUT</strong></p>
                                    <strong>(Documentos Obligatorio)</strong>
                                    </label>
                                    <input type="file" class="form-control-file" name="archivo_rut" required>
                                </div> 

                            </div>

                            <div class="form-group">

                                <div class="form-check">

                                    <p>
                                    Autoriza a el tratamiento de acuerdo con los fines establecidos y la seguridad y privacidad de la información que recolecte, almacene, 
                                    use, circule o suprima, que contenga datos personales y en cumplimiento del mandato legal, establecido en la Constitución Política de 
                                    Colombia(arts. 15 y 20), la Ley 1581 de 2012 "por la cual se dictan disposiciones generales para la protección de datos personales" y 
                                    el Decreto 1377 de 2013 "por el cual se reglamenta parcialmente la Ley 1581 de 2012" y al compromiso institucional en cuanto al tratamiento de la información. 
                                    </p>

                                    <input class="form-check-input" type="checkbox" name="gridCheck"  required style="margin-left: 5px;">
                                    <label class="form-check-label" for="gridCheck" style="margin-left: 20px;">
                                        Acepto
                                    </label>

                                </div>

                            </div>

                            <button type="submit" class="btn btn-primary" style="display: block; margin:auto;" name="enviar" id="enviar">Enviar</button>

                            
                        </form>


                    </div>

                </div>

                <!--fin registro-->


                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">


                </div>


            </div>


           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
             <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: black;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: black;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: black;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: black;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="js/sweetAlert.js"></script>

  </body>
  
</html>