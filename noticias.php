<?php
include('header.php');
?>

           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">

                        <div class="title2">
                            <h2 class="t2"><strong>Noticias</strong></h2>
                        </div>
                        
                        <br>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                    Publicación de noticias por fecha, recopilar información publicada en medios de comunicación y Corpocesar, sobre negocios verdes, producción y consumo sostenible.
                    </p>


                    <!--tablas-->

                    <table class="table table-striped">

                        <thead>

                            <tr>

                                <th scope="col" style="text-align: justify; text-align: center;">#</th>

                                <th scope="col" style="text-align: center;">FECHA</th>

                                <th scope="col" style="text-align: center;">NOMBRE</th>

                                <th scope="col">LINK </th>

                            </tr>

                        </thead>

                        <tbody>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">1</th>

                                <td style="text-align: justify; text-align: center;">18 de Septiembre de 2020</td>
                                
                                <td style="text-align: justify; text-align: center;">Con seis iniciativas, 
                                Corpocesar participará de la Feria de 
                                Negocios Verdes en casa 2020.</td>

                                <td><a href="https://domiplay.co/video/con-seis-iniciativas-corpocesar-participara-18-09-20-rta-uqscmw" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>
                            
                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">2</th>

                                <td style="text-align: justify; text-align: center;">16 de Septiembre de 2020</td>

                                <td style="text-align: justify; text-align: center;">
                                Feria de Negocios Verdes de la Región Caribe.</td>

                                <td><a href="http://www.expresionnaranja.com/feria-de-negocios-verdes-de-la-region-caribe/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">3</th>

                                <td style="text-align: justify; text-align: center;">15 de Septiembre de 2020</td>

                                <td style="text-align: justify; text-align: center;">
                                Negocios verdes del Cesar participarán en Feria de Negocios Verdes Caribe.</td>

                                <td><a href="https://www.enfoquevallenato.com/negocios-verdes-del-cesar-participaran-en-feria-de-negocios-verdes-caribe/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">4</th>

                                <td style="text-align: justify; text-align: center;">12 de Septiembre de 2020</td>

                                <td style="text-align: justify; text-align: center;">
                                Región Caribe promueve feria de negocios verdes.</td>

                                <td><a href="https://www.puroambienteinformativo.com/2020/09/12/region-caribe-promueve-feria-de-negocios-verdes/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">5</th>

                                <td style="text-align: justify; text-align: center;">9 de Septiembre de 2020</td>

                                <td style="text-align: justify; text-align: center;">
                                Feria de Negocios Verdes Región Caribe: una gran plataforma para nuestra reactivación.</td>

                                <td><a href="https://atlinnovacion.com/2020/09/09/feria-de-negocios-verdes-region-caribe-una-gran-plataforma-para-nuestra-reactivacion-2/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>
                            
                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">6</th>

                                <td style="text-align: justify; text-align: center;">8 de Diciembre de 2019</td>

                                <td style="text-align: justify; text-align: center;">
                                Corpocesar y Minambiente certificaron 30 Negocios Verdes.</td>

                                <td><a href="https://elpilon.com.co/corpocesar-y-minambiente-certificaron-30-negocios-verdes/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">7</th>

                                <td style="text-align: justify; text-align: center;">4 de Diciembre de 2019</td>

                                <td style="text-align: justify; text-align: center;">
                                Corpocesar certificó 30 Negocios Verdes.</td>

                                <td><a href="https://www.corpocesar.gov.co/Corpocesar-certifico-30-Negocios-Verdes-.html" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">8</th>

                                <td style="text-align: justify; text-align: center;">23 de Octubre de 2019</td>

                                <td style="text-align: justify; text-align: center;">
                                El Cesar con cinco Negocios Verdes, presente con éxito en Bioexpo 2019.</td>

                                <td><a href="https://www.maravillastereo.com/el-cesar-con-cinco-negocios-verdes-presente-con-exito-en-bioexpo-2019/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">9</th>

                                <td style="text-align: justify; text-align: center;">22 de Octubre de 2019</td>

                                <td style="text-align: justify; text-align: center;">
                                Cinco Negocios Verdes representaron al departamento del Cesar en Bioexpo 2019.</td>

                                <td><a href="https://www.corpocesar.gov.co/Cinco-Negocios-Verdes-representaron-al-departamento-del-Cesar-en-Bioexpo-2019.html" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                            <tr>

                                <th scope="row" style="text-align: justify; text-align: center;">10</th>

                                <td style="text-align: justify; text-align: center;">21 de Octubre de 2019</td>

                                <td style="text-align: justify; text-align: center;">
                                Cinco negocios verdes del Cesar estuvieron en Bioexpo 2019.</td>

                                <td><a href="https://elpilon.com.co/cinco-negocios-verdes-del-cesar-estuvieron-en-bioexpo-2019/" 
                                class="btn btn-primary" target="_banck" style="text-align: center;">Acceder</a></td>

                            </tr>

                        </tbody>

                    </table>

                     <!--fin tablas-->

                     <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">


                </div>

                

            </div>


           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>