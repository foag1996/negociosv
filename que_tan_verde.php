<?php
include('header.php');
?>


           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                    <img src="img/q1.png" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid"> <br>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                    La Oficina de Negocios Verdes y Sostenibles del MADS, validó de manera participativa, con la intervención de diferentes 
                    autoridades ambientales, una herramienta de verificación de los criterios de negocios verdes. Una vez aplicada al interior 
                    de una empresa o iniciativa, se logra establecer un nivel de avance en cuanto a la implementación de los doce criterios, lo 
                    que permite definir acciones que llevarán a un mejoramiento continuo.
                    </p>

                    <p>Para las primeras fases de aplicación de la herramienta el MADS acompañó a varias autoridades ambientales a través de la GIZ 
                    y Biocomercio Colombia, para medir diferentes iniciativas en distintas partes del territorio nacional, de tal manera que conformaron
                    una base de datos de empresas que alcanzaron niveles ascendentes de un negocio verde: satisfactorio, avanzado, e ideal.</p>

                    <div class="container">

                        <div class="row">

                            <div class="col-sm"
                            style="text-align: justify; border-radius:15px; border: 
                            solid #707070; margin-top:20px; margin-left:20px; margin-right:20px;">

                                <div class="title2">
                                    <h2 class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Negocio Verde Satisfactorio</strong></h2>
                                </div>

                                <br>

                                <p style="text-align: justify;">
                                El bien o servicio cumple con más de la mitad de los criterios esenciales de Negocios Verdes. Se puede clasificar
                                como Negocio Verde. No obstante, se deberán tomar acciones de fortalecimiento de aquellos criterios con puntaje 0.5 
                                y comenzar acciones para lograr el cumplimiento de aquellos criterios con puntaje 0, de tal forma que se pueda clasificar como un Negocio Verde 100%.
                                </p>

                            </div>

                            <div class="col-sm"
                            style="text-align: justify; border-radius:15px; border: 
                            solid #707070; margin-top:20px; margin-left:20px; margin-right:20px;">

                                <div class="title2">
                                    <h2 class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Negocio Verde Avanzado</strong></h2>
                                </div>

                                <br>

                                <p style="text-align: justify;">
                                El bien o servicio cumple con el 80% de los criterios esenciales de Negocios Verdes. 
                                Se pretende que se siga un mejoramiento continuo para cumplir a cabalidad con la totalidad de los criterios.
                                </p>

                            </div>

                            <div class="col-sm"
                            style="text-align: justify; border-radius:15px; border: 
                            solid #707070; margin-top:20px; margin-left:20px; margin-right:20px;">

                                <div class="title2">
                                    <h2 class="t2" style="background-color: #0093D8; margin-top:20px;"><strong>Negocio Verde Ideal</strong></h2>
                                </div>

                                <br>

                                <p style="text-align: justify;">
                                El bien o servicio cumple con más del 80% o con la totalidad de los criterios esenciales de Negocios Verdes, 
                                además cumple con algún o todos los criterios adicionales. Se puede clasificar como Negocio Verde Ideal. Se 
                                pretende que se siga un mejoramiento continuo para cumplir a cabalidad con la totalidad de los criterios de cumplimiento y adicionales.
                                </p>


                            </div>

                        </div>

                    </div>

                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">


                </div>

                

            </div>


           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>