<?php
include('header.php');
?>



            <!--titulo1-->

            <div class="title1">
                <h1 class="t1"><strong>Negocios verdes</strong></h1>
            </div>
               

            <!--titulo-->

            <!--corusel-->

            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >

                <ol class="carousel-indicators">

                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>

                </ol>

                <div class="carousel-inner"> 

                    <div class="carousel-item active">
                        <img src="img/c1.jpg" class="d-block w-100 sld" alt="..."  width="560px" height="550px">
                    </div>

                    <div class="carousel-item">
                        <img src="img/c2.jpg" class="d-block w-100 sld" alt="..."  width="560px" height="550px">
                    </div>

                    <div class="carousel-item">
                        <img src="img/c3.jpg" class="d-block w-100 sld" alt="..."  width="560px" height="550px">
                    </div>

                    <div class="carousel-item">
                        <img src="img/c4.jpg" class="d-block w-100 sld" alt="..."  width="560px" height="550px">
                    </div>

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

            <!-- fin corusel--> 

            <hr>

           <!-- negocios verdes-->

            <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <h2 class="t2"><strong>¿Qué son los negocios verdes?</strong></h2>
                    </div>

                    <div class="container-content">

                        <div class="content">
                             <p>
                               Contempla las actividades económicas en las que se ofertan bienes o servicios, que generan impactos ambientales positivos y además incorporan buenas prácticas ambientales, sociales y económicas con enfoque de ciclo de vida, contribuyendo a la conservación del ambiente como capital natural que soporta el desarrollo del territorio
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                              Promueve patrones de producción y consumo sostenibles de bienes y servicios de los negocios verdes y sostenibles.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                              Propicia la creación de una cultura alineada con principios ambientales, sociales y éticos.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                              Facilita la toma de decisiones a los consumidores (públicos o privados) al momento de elegir un bien y servicio.
                            </p>

                            <p><i class="fas fa-plus-circle" style="color: #0093D8;"></i>
                              Visibiliza una oferta de bienes y servicios de cara al mercado nacional e internacional.
                            </p>

                       </div>

                            <div class="container-img">
                              <img src="img/l2.PNG" alt="" class="img-fluid l2">
                            </div>

                    </div>

                </div>

            </div>

           <!--fin negocios verdes-->

           <hr>

            <!--cards-->

            <div class="negocios">

                <div class="container-description">

                <div class="title3">
                    <h1 class="t3"><strong>Renuevo para el Desarrollo Sostenible</strong></h1>
                </div>

                <div class="container-cards" style="display: flex;">

                    <!-- Columns are always 50% wide, on mobile and desktop -->
                    <div class="row">

                      <div class="col-md-6">

                        <div class="card card1" style="width: 25rem" style="display: flex;">

                            <div class="card-body">
                                
                                <h3 class="card-title" align="center">Portafolio</h3>
                                
                            </div>

                            <img src="img/d1.jpg" class="card-img img-fluid" alt="...">
                            <div class="card-body">
                              <p class="card-text">El portafolio de negocios verdes y sostenibles del departamento del cesar 2021, es la compilación de información  sobre negocios verdes identificados, verificaos y certificados por la oficina de Negocios verdes, ministerio de ambiente y desarrollo sostenible y la corporación autónoma regional del cesar. </p>

                              <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-leer" data-toggle="modal" data-target="#masi1">
                                    Mas informacíon
                                </button>
                              <!-- Button trigger modal -->

                            </div>
                          </div>

                      </div>

                      <div class="col-md-6">

                        <div class="card card2" style="width: 25rem;" style="display: flex;">

                            <div class="card-body">
                                
                                <h3 class="card-title" align="center">Guía técnica de turismo</h3>
                                
                            </div>

                            <img src="img/d2.jpg" class="card-img img-fluid" alt="...">
                            <div class="card-body">
                              <p class="card-text">Desde el punto de vista de los Negocios Verdes, el turismo de la naturaleza o ecoturismo, es un subsector involucrado con biocomercio, sector que hace parte de la categoría de bienes y servicios. sostenibles provenientes de los recursos naturales, administrados por corpocesar.</p>
                              
                              <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-leer" data-toggle="modal" data-target="#masi2">
                                    Mas informacíon
                                </button>
                              <!-- Button trigger modal -->

                            </div>
                          </div>

                      </div>

                    </div>
                  </div>

                </div>

            </div>

                

            <!--fin cards-->

            <hr>
  
            <!-- Modal -->

            <div class="modal fade" id="masi1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="masi1" aria-hidden="true">
                
                <div class="modal-dialog">

                    <div class="modal-content">

                           
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="align-items: flex-end;">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        

                        <div class="modal-body">
                            <img src="img/m1.PNG" class="img-fluid" alt="">
                            <a href="pdf/portafolio negocios verdes cesar, corpocesar.pdf" class="btn btn-primary" download="portafolio negocios verdes.pdf" 
                            style="display: block; width: 30%; text-align:center; margin: auto;">
                                Descargar
                            </a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="modal fade" id="masi2" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="masi2" aria-hidden="true">
                
                <div class="modal-dialog">

                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">Guía técnica de turismo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                        </div>
                            
                        <div class="modal-body"> 
                            <img src="img/m2.PNG" class="img-fluid" alt="" style="text-align: center;">
                            <a href="pdf/guia_turistica_negocios_verde_lectura_compressed.pdf" class="btn btn-primary" download="guia_turistica_negocios_verde_lectura_compressed.pdf"
                                style="display: block; width: 30%; text-align:center; margin: auto;">
                                Descargar
                            </a>
                        </div>

                    </div>

                </div>

            </div>

            <!--Fin Modal -->

            <!--Aprendamos negocios verdes-->

                <div class="aprendamos">

                    <div class="container-description">

                        <div class="title4">
                            <h2 class="t4"><strong>Aprendamos sobre Negocios Verdes y Sostenibles</strong></h2>
                        </div>

                        <div class="container-content">

                            <div class="content">

                                <p style="margin-top: 20px;">
                                    La Oficina de Negocios Verdes y Sostenibles del Ministerio de Ambiente pone a tu disposición este video para conocer cómo funcionan este tipo de iniciativas y cómo pueden ser más competitivas implementando acciones amigables con el medio ambiente.
                                </p>

                                <a href="https://www.minambiente.gov.co/index.php/negocios-verdes-y-sostenibles" class="btn btn-primary btn-mas" target="_banck">Mas informacíon</a>

                        </div>

                                <div class="container-video">
                                    <iframe class="video1 video-fluid " style="margin: 10px 10px 10px 10px;" width="560" height="315" src="https://www.youtube.com/embed/rfD-UKuod18" 
                                    frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                                    allowfullscreen></iframe>
                                </div>

                        </div>

                    </div>

                </div>

         <!--Fin aprendamos negocios verdes-->

         

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>