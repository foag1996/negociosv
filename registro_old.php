<?php
include('header.php');
?>


           <!-- negocios verdes-->

           <div class="negocios">

                <div class="container-description">

                    <div class="title2">
                        <img src="img/r1.png" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid"> <br>
                    </div>

                    <br>
 
                    <p style="text-align: justify;">
                    Para Iniciar el proceso de identificación de su iniciativa, por parte de la Ventanilla de Negocios Verdes de Corpocesar, 
                    le pedimos el favor pueda diligenciar el siguiente formulario.
                    </p>
                    
                <!--registro-->

                <div class="negocios">

                    <div class="container-description">
                        

                        <form action="registrar.php" method="POST" enctype="multipart/form-data">

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="">Nombres</label>
                                    <input type="text" class="form-control" name="nombre" placeholder="Nombres" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Apellidos</label>
                                    <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="">N° de Identificacion</label>
                                    <input type="number" class="form-control" name="cedula" placeholder="N° de Identificacion" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">N° de Celular</label>
                                    <input type="number" class="form-control" name="celular" placeholder="N° de Celular" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="">Correo</label>
                                    <input type="email" class="form-control" name="correo" placeholder="Correo" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Dirección </label>
                                    <input type="text" class="form-control" name="direccion" placeholder="Dirección" required>
                                </div>


                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="">Razon Social</label>
                                    <input type="text" class="form-control" name="razon" placeholder="Razon Social" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Registro unico tributario (RUT)</label>
                                    <input type="text" class="form-control" name="rut" placeholder="RUT xxx.xxx.xxx-x" required>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="">Descripción de actividades económicas</label>
                                    <textarea name="actividad" class="form-control" rows="5" placeholder="Descripción de actividades económicas" required></textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="">Descripción de bienes y servicios ofertados</label>
                                    <textarea name="servicio" class="form-control" rows="5" placeholder="Descripción de bienes y servicios ofertados" required></textarea>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="exampleFormControlFile1">
                                    <p>Adjuntar en un solo Archivo:</p>
                                    Certificado de camara de comercio y RUT
                                    </label>
                                    <input type="file" class="form-control-file" name="archivo" required>
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="form-check">

                                    <p>
                                    Autoriza a el tratamiento de acuerdo con los fines establecidos y la seguridad y privacidad de la información que recolecte, almacene, 
                                    use, circule o suprima, que contenga datos personales y en cumplimiento del mandato legal, establecido en la Constitución Política de 
                                    Colombia(arts. 15 y 20), la Ley 1581 de 2012 "por la cual se dictan disposiciones generales para la protección de datos personales" y 
                                    el Decreto 1377 de 2013 "por el cual se reglamenta parcialmente la Ley 1581 de 2012" y al compromiso institucional en cuanto al tratamiento de la información. 
                                    </p>

                                    <input class="form-check-input" type="checkbox" name="gridCheck"  required style="margin-left: 5px;">
                                    <label class="form-check-label" for="gridCheck" style="margin-left: 20px;">
                                        Acepto
                                    </label>

                                </div>

                            </div>

                            <button type="submit" class="btn btn-primary" style="display: block; margin:auto;" name="enviar" id="enviar">Enviar</button>

                            
                        </form>


                    </div>

                </div>

                <!--fin registro-->


                    <img src="img/p1.PNG" alt="" style="width: auto; height:auto; display:block; margin: auto;" class="img-fluid">


                </div>


            </div>


           <!--fin negocios verdes-->

           <hr>

           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="js/sweetAlert.js"></script>

  </body>
  
</html>