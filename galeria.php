<?php
include('header.php');
?>
    
         <div class="negocios">

                <div class="container-description">   

                <div class="title1">
                     <h1 class="t1"><strong>Fotos</strong></h1>
                </div>

         <div class="gallery-container">
             <div class="gallery__item">
                 <img src="img/g1.png" alt="" class="gallery__img img-fluid">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g2.png" alt="" class="gallery__img">
                
             </div>
             <div class="gallery__item">
                 <img src="img/g3.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g4.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g5.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g6.png" alt="" class="gallery__img">

             </div>
             <div class="gallery__item">
                 <img src="img/g7.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g8.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g9.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g10.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g11.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g12.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g13.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g14.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g15.png" alt="" class="gallery__img">
                 
             </div>
             <div class="gallery__item">
                 <img src="img/g16.png" alt="" class="gallery__img">
                 
             </div>

         </div>

                </div>
         </div> 

         <hr>

         <div class="negocios">

            <div class="container-description">

            <div class="title1">
                <h1 class="t1"><strong>Videos</strong></h1>
            </div>

            <div class="container">

               <div class="row">

                    <div class="col-sm-4">

                        <div class="container-video" style="margin-top: 10px;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/8-xCZSCb430" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>

                    <div class="col-sm-4">

                       <div class="container-video" style="margin-top: 10px;">
                       <iframe width="560" height="315" src="https://www.youtube.com/embed/MXajgbnfLpM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="container-video" style="margin-top: 10px;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/qyPPpmgBCss" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>

                    </div>

               </div>

               <div class="row">

                    <div class="col-sm-4">

                        <div class="container-video" style="margin-top: 10px;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/_C6kc48c72g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                        </div>

                    </div>

                    <div class="col-sm-4">

                       <div class="container-video" style="margin-top: 10px;">
                       <iframe width="560" height="315" src="https://www.youtube.com/embed/zNGizcmcJ7k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="container-video" style="margin-top: 10px;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/FbJkwMDVFEU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                        </div>

                    </div>

               </div>

           </div>


            </div>

         </div>
         



        <hr>

           


           <!-- Footer -->
           <?php
               include('footer.php');
           ?>
           <!-- Footer -->

             <!-- redes sociales -->

             <div class="social-bar">
                <a href="https://www.facebook.com/negociosverdes.cesar" class="icon icon-facebook" target="_blank"><i class="fab fa-facebook-f" style="color: white;"></i></a>
                <a href="https://twitter.com/NegVerdesCesar" class="icon icon-twitter" target="_blank"><i class="fab fa-twitter" style="color: white;" ></i></a>
                <a href="https://www.youtube.com/channel/UCjvgDW_vZ_EQhrx31tF5hJg" class="icon icon-youtube" target="_blank"><i class="fab fa-youtube" style="color: white;"></i></a>
                <a href="https://www.instagram.com/negociosverdescesar/" class="icon icon-instagram" target="_blank"><i class="fab fa-instagram" style="color: white;"></i></a>
              </div>
             
             <!-- fin redes sociales -->
            

        </div> <!-- fin contenedor-->

    

    <!-- Option 2: Separate Popper and Bootstrap JS -->
   
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js" integrity="sha512-UwcC/iaz5ziHX7V6LjSKaXgCuRRqbTp1QHpbOJ4l1nw2/boCfZ2KlFIqBUA/uRVF0onbREnY9do8rM/uT/ilqw==" crossorigin="anonymous"></script>
    
  </body>
</html>