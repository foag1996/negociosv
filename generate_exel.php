<?php
require ('./vendor/autoload.php');


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;


function makeExcelContact($nombre, $apellidos, $correo, $celular, $direccion, $ciudad, $comentario){
    $spreadsheet = new Spreadsheet();
    $array_data = [
        [NULL, 'Nombre', 'Apellidos', 'Correo', 'Celular', 'Direccion', 'Ciudad'],
        ['Datos',   $nombre,   $apellidos,  $correo, $celular, $direccion,$ciudad],
        
    ];
    $spreadsheet->getActiveSheet()->fromArray($array_data, NULL, 'A1');
    $spreadsheet->getActiveSheet()->setCellValue('A4', 'Comentario');
    $spreadsheet->getActiveSheet()->setCellValue('B4', $comentario);
    $spreadsheet->getActiveSheet()
        ->getStyle('A2:A8')
        ->getFill()
        ->setFillType(
            \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('F7FFC1');
    $spreadsheet->getActiveSheet()
            ->getStyle('A1:H1')
            ->getFill()
            ->setFillType(
                \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('C4E1FF');
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(25.45);
    
    $writer = new Xlsx($spreadsheet);
    $path = 'contactos/nuevo_contacto'.time().'.xlsx';
    $writer->save($path);
    return $path;
}

function makeExcelRegister($nombre,$apellidos,$cedula,$celular,$correo,$direccion,$razon,$rut,$actividad,$servicio,$ruta,
                            $facebook, $instagram, $twitter,$pagina_web, $youtube, $ingresos2019, $ingresos2020, $ruta2){
    $spreadsheet = new Spreadsheet();
    $file_name = explode('/', $ruta);
    $path_to_file = DOMINIO.'descargas.php?archivos='.$file_name[1];
    $file_name2 = explode('/', $ruta2);
    $path_to_file2 = DOMINIO.'descargas.php?archivos='.$file_name2[1];
    
    $array_data = [
        [NULL, 'Nombre', 'Apellidos','Cedula', 'Correo', 'Celular', 'Direccion', 'Razón', 'Rut', 'Actividad', 'Servicio', 'Archivo C.Comercio',
                'Facebook', 'Instagram', 'Twitter', 'Pagina Web', 'Youtube','Valores 2019', 'Valores 2020', 'Archivo RUT'],
        ['Datos',   $nombre,   $apellidos, $cedula, $correo, $celular, $direccion,$razon,$rut,$actividad,$servicio,$path_to_file,
        $facebook, $instagram, $twitter,$pagina_web, $youtube, $ingresos2019, $ingresos2020, $path_to_file2],        
    ];

    $spreadsheet->getActiveSheet()
    ->getStyle('A2:A8')
    ->getFill()
    ->setFillType(
        \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('F7FFC1');
    $spreadsheet->getActiveSheet()
            ->getStyle('A1:T1')
            ->getFill()
            ->setFillType(
                \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('C4E1FF');
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('R')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('S')->setWidth(25.45);
    $spreadsheet->getActiveSheet()->getColumnDimension('T')->setWidth(25.45);
        

    $spreadsheet->getActiveSheet()->fromArray($array_data, NULL, 'A1');
    $writer = new Xlsx($spreadsheet);
    $path = 'registros/nuevo_registro_'.time().'.xlsx';
    $writer->save($path);
    return $path;

}